import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TasksComponent} from './pages/tasks/tasks.component';
import {AboutComponent} from './pages/about/about.component';


const routes: Routes = [
  {
    path: 'tasks',
    component: TasksComponent,
  },
  {
    path: '',
    redirectTo: '/tasks',
    pathMatch: 'full'
  },
  {
    path: 'about',
    component: AboutComponent,
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
