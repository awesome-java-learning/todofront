export class Task {
  constructor(
    public id: number,
    public name: string,
    public completed: boolean,
    public creationDate: Date,
    public completionDate: Date,
  ) {
  }
}
