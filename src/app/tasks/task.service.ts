import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  constructor(private http: HttpClient) {
  }

  public search(name: string, completed: boolean, creationDate: Date, completionDate: Date) {
    console.log('taskService.search');

    let params = new HttpParams();
    if (name != null) {
      params = params.append('name', name);
    }
    if (completed != null) {
      params = params.append('completed', String(completed));
    }
    if (creationDate != null) {
      params = params.append('creationDate', String(creationDate));
    }
    if (completionDate != null) {
      params = params.append('completionDate', String(completionDate));
    }

    return this.http.get('/api/tasks', {params: params});
  }

  public delete(id: number) {
    console.log('taskService.delete');
    return this.http.delete('/api/tasks/' + id);
  }

  public update(id: number, name: string, completed: boolean,
                creationDate: Date, completionDate: Date,
  ) {
    console.log('taskService.update');

    if (completed) {
      completionDate = new Date();
    } else {
      completionDate = undefined;
    }

    return this.http.put('/api/tasks/' + id,
      {
        "name": name, "completed": completed, "id": id,
        "creationDate": creationDate, "completionDate": completionDate
      });
  }
}
