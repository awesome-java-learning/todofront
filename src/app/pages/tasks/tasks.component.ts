import {Component, OnInit} from '@angular/core';
import {TaskService} from '../../tasks/task.service';
import {Task} from '../../tasks/task';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {
  public tasks: Task[] = null;

  constructor(private taskService: TaskService) {
  }

  ngOnInit() {
    console.log('ngOnInit');
    this.search(null,null,null,null,null);
  }

  private search(id: number, name: string, completed: boolean,
                 creationDate: Date, completionDate: Date): void {
    const tasks = this.taskService.search(name, completed, creationDate, completionDate);
    tasks.subscribe((tarray: Task[]) => {
      this.tasks = tarray;
    });
  }

  public delete(id: number): void {
    console.log('delete!', id);
    this.taskService.delete(id).subscribe(_ => {
      console.log('delete success');
      this.search(null,null,null,null,null);
    });
  }

  public update(id: number, name: string, completed: boolean,
                creationDate: Date, completionDate: Date,
  ): void {
    console.log('update', id);
    this.taskService.update(id, name, completed, creationDate, completionDate).subscribe(_ => {
      console.log('update success');
      this.search(null,null,null,null,null);
    });
  }
}
